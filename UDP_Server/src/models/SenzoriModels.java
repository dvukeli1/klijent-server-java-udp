package models;

/**
 * 
 * @author Dimitrij Vukelić
 * <{@code}> atributi i metode modela
 */

public class SenzoriModels {
	
	private String sliderX="0";
	private String sliderY="0";
	private String Ime="";
	private boolean provjera = false;
	private int visEkr = 0;
	private int sirEkr = 0;

	

	public String getIme() {
		return Ime;
	}

	public void setIme(String ime) {
		Ime = ime;
	}

	public boolean isProvjera() {
		return provjera;
	}

	public void setProvjera(boolean provjera) {
		this.provjera = provjera;
	}

	public String getSliderX() {
		return sliderX;
	}

	public void setSliderX(String sliderX) {
		this.sliderX = sliderX;
	}

	public String getSliderY() {
		return sliderY;
	}

	public void setSliderY(String sliderY) {
		this.sliderY = sliderY;
	}

	public synchronized int getVisEkr() {
		return visEkr;
	}

	public synchronized void setVisEkr(int visEkr) {
		this.visEkr = visEkr;
	}

	public synchronized int getSirEkr() {
		return sirEkr;
	}

	public synchronized void setSirEkr(int sirEkr) {
		this.sirEkr = sirEkr;
	}

}
