package controllers;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.InputEvent;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import models.SenzoriModels;

public class UDPServerControllers {

	//kreiranje instance modela
	static private SenzoriModels senzori = new SenzoriModels();


	public SenzoriModels getSenzori() {
		return senzori;
	}


	public static void setSenzori(SenzoriModels senzori) {
		UDPServerControllers.senzori = senzori;
	}


	/**
	 * @param args
	 * @throws AWTException 
	 */

	public void main(String[] args) throws IOException, AWTException {
		// TODO Auto-generated method stub
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

		senzori.setSirEkr(dim.width);
		senzori.setVisEkr(dim.height);

		System.out.println("ovo sam dobio " + dim.width);

		//Server port
		int port = 50001;

		byte[] rcvBuf = new byte[256]; // Atribut prihvacanja podataka
		byte[] sendBuf = new byte[256];// Atribut slanja podataka
		String receivedString;

		//Novi UDP socket koji je povezan za zadani port na mrezi

		InetAddress address = InetAddress.getLocalHost();
		DatagramSocket socket = new DatagramSocket( port );

		senzori.setIme(address.getHostName());


		int x = 0;
		int y = 0;

		//kreiranje Robot metoda za manipulaciju misem
		final Robot mouse = new Robot();

		// Beskonacna petlja
		while(true ){

			/**
			 * Reset vrijednosti na 0
			 */

			senzori.setSliderX("0");
			senzori.setSliderY("0");
			String provjera = "0";
			String lclick ="0";
			String rclick ="0";

			//Kreira DatagramPacket za primanje podataka
			DatagramPacket packet =	new DatagramPacket(rcvBuf,rcvBuf.length);
			senzori.setProvjera(false);
			//Prihvacanje podataka
			socket.receive( packet ); 

			//System.out.println("ovo sam dobio " + senzori.getSliderX());

			receivedString = new String(packet.getData(),packet.getOffset(),
					packet.getLength() );
			String recive = receivedString.toString();

			/**
			 * Rastavljanje ulaza koristeci delimiter ";" i spremanje u polje stringova
			 */

			String delimiter = ";";
			String[] primljeno = recive.split(delimiter);

			provjera = primljeno[2];

			/**
			 * Kreiranje podataka za mouse move
			 */

			/* Provjerava ispravnost paketa, treca brojcana vrijednost u datagramu  je 1 */
			if(provjera.contains("1")){

				lclick = primljeno[3];
				rclick = primljeno[4];

				// Math.round - zaokruzivanje na cijele brojeve
				senzori.setSliderX(String.valueOf(Math.round(Float.valueOf(primljeno[0]))));
				senzori.setSliderY(String.valueOf(Math.round(Float.valueOf(primljeno[1]))));

				x = x - Integer.parseInt(senzori.getSliderX());
				y = y + Integer.parseInt(senzori.getSliderY());

				/* Limitiranje vrijednosti x i y osi*/
				if(x <= 0) x = 0;
				if(x >= senzori.getSirEkr()) x=senzori.getSirEkr();

				if(y <= 0) y = 0;
				if(y >= senzori.getVisEkr()) y=senzori.getVisEkr();
				
				//metoda postavljanja vrijednosti x i y pozicije misa (*3 je povecanje brzine)
				mouse.mouseMove(x*3, y*3);

				provjera="0";


				if (lclick.contains("1")){
					mouse.mousePress(InputEvent.BUTTON1_MASK);
					mouse.mouseRelease(InputEvent.BUTTON1_MASK);
				}

				if (rclick.contains("1")){
					mouse.mousePress(InputEvent.BUTTON3_MASK);
					mouse.mouseRelease(InputEvent.BUTTON3_MASK);
				}


			}

			/**
			 * Slanje podataka u klijent
			 */

			/*//Kreira DatagramPacket za slanje podataka
			String slanje = new String ("OK");
			sendBuf = slanje.getBytes();
			//System.out.println(packet.getAddress());

			DatagramPacket sendPacket = new DatagramPacket( sendBuf, sendBuf.length, packet.getAddress(),
					50002);
			//send packet
			System.out.println(slanje);
			socket.send(sendPacket); */

		}
	}
}
