package views;

import java.awt.AWTException;
import java.awt.EventQueue;
import java.io.IOException;

import javax.swing.JFrame;

import controllers.UDPServerControllers;
import javax.swing.JLabel;

import models.SenzoriModels;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class KlijentViews {

	private JFrame frame;
	UDPServerControllers start = new UDPServerControllers();
	SenzoriModels senzori = start.getSenzori();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					KlijentViews window = new KlijentViews();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public KlijentViews() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		

		Thread prihvat = new Thread(new Runnable() {
			public void run() {
			try {
				start.main(null);
			} catch (IOException | AWTException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
			}
		});prihvat.start();
		
		
		frame = new JFrame();
		frame.setBounds(100, 100, 863, 514);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("Rezolucija = " + senzori.getSirEkr()
				+ "*" + senzori.getVisEkr());
		lblNewLabel_1.setBounds(10, 0, 211, 15);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblDodji = new JLabel("Dodji");
		lblDodji.setFont(new Font("Impact", Font.PLAIN, 14));
		lblDodji.setBounds(354, 57, 38, 18);
		frame.getContentPane().add(lblDodji);
		
		final JLabel label = new JLabel("Dodji");
		label.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				label.setText("OK");
			}
		});
		
		label.setFont(new Font("Impact", Font.PLAIN, 14));
		label.setBounds(207, 107, 38, 18);
		frame.getContentPane().add(label);
		
		JLabel label_1 = new JLabel("Dodji");
		label_1.setFont(new Font("Impact", Font.PLAIN, 14));
		label_1.setBounds(289, 152, 38, 18);
		frame.getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("Dodji");
		label_2.setFont(new Font("Impact", Font.PLAIN, 14));
		label_2.setBounds(403, 187, 38, 18);
		frame.getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("Dodji");
		label_3.setFont(new Font("Impact", Font.PLAIN, 14));
		label_3.setBounds(179, 287, 38, 18);
		frame.getContentPane().add(label_3);
		
		JLabel label_4 = new JLabel("Dodji");
		label_4.setFont(new Font("Impact", Font.PLAIN, 14));
		label_4.setBounds(440, 347, 38, 18);
		frame.getContentPane().add(label_4);
		
		JLabel label_5 = new JLabel("Dodji");
		label_5.setFont(new Font("Impact", Font.PLAIN, 14));
		label_5.setBounds(567, 233, 38, 18);
		frame.getContentPane().add(label_5);
		
		JLabel label_6 = new JLabel("Dodji");
		label_6.setFont(new Font("Impact", Font.PLAIN, 14));
		label_6.setBounds(622, 85, 38, 18);
		frame.getContentPane().add(label_6);
		
		JLabel label_7 = new JLabel("Dodji");
		label_7.setFont(new Font("Impact", Font.PLAIN, 14));
		label_7.setBounds(691, 351, 38, 18);
		frame.getContentPane().add(label_7);
		
		JLabel label_8 = new JLabel("Dodji");
		label_8.setFont(new Font("Impact", Font.PLAIN, 14));
		label_8.setBounds(83, 400, 38, 18);
		frame.getContentPane().add(label_8);
		
		
		
		
		
	}
}
