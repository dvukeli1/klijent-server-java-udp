package models;


public class SenzoriModels {
	
	/**
	 * Definiranje varijabli prihvata senzora
	 */
	
	private String pomak_x = "0";
	private String pomak_y = "0";
	private String pomak_z = "0";
	
	private float korekcija_x = 0;
	private float korekcija_y = 0;
	private float korekcija_z = 0;
	
	private int lClick = 0;
	private int rClick = 0;
	
	
	/**
	 * Generirane metode get i set 
	 */
	
	public String getPomak_x() {
		return pomak_x;
	}
	
	public void setPomak_x(String pomak_x) {
		this.pomak_x = pomak_x;
	}
	
	public String getPomak_y() {
		return pomak_y;
	}
	
	public void setPomak_y(String pomak_y) {
		this.pomak_y = pomak_y;
	}
	public String getPomak_z() {
		return pomak_z;
	}
	public void setPomak_z(String pomak_z) {
		this.pomak_z = pomak_z;
	}
	
	public synchronized float getKorekcija_x() {
		return korekcija_x;
	}

	public synchronized void setKorekcija_x(float korekcija_x) {
		this.korekcija_x = korekcija_x;
	}

	public synchronized float getKorekcija_y() {
		return korekcija_y;
	}

	public synchronized void setKorekcija_y(float korekcija_y) {
		this.korekcija_y = korekcija_y;
	}

	public synchronized float getKorekcija_z() {
		return korekcija_z;
	}

	public synchronized void setKorekcija_z(float korekcija_z) {
		this.korekcija_z = korekcija_z;
	}

	public synchronized int getlClick() {
		return lClick;
	}

	public synchronized void setlClick(int lClick) {
		this.lClick = lClick;
	}

	public synchronized int getrClick() {
		return rClick;
	}

	public synchronized void setrClick(int rClick) {
		this.rClick = rClick;
	}
	
	

}
