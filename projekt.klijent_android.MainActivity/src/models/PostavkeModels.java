package models;

import projekt.klijent_android.R;


/**
 * 
 * @author Dimitrij Vukelić
 * 
 */

public class PostavkeModels {
	
	/**
	 * Definiranje varijabli konekcije klijenta i provjere
	 */
	
	private String server ="192.168.1.108";
	private boolean spajanje = false;
	private String potvrda ="";
	
	/**
	 * Generirane metode get i set 
	 */
	
	public synchronized String getServer() {
		return server;
	}
	public synchronized void setServer(String server) {
		this.server = server;
	}
	public synchronized boolean isSpajanje() {
		return spajanje;
	}
	public synchronized void setSpajanje(boolean spajanje) {
		this.spajanje = spajanje;
	}
	public synchronized String getPotvrda() {
		return potvrda;
	}
	public synchronized void setPotvrda(String potvrda) {
		this.potvrda = potvrda;
	}
	

}
