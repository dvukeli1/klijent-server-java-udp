package controllers;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import sun.jdbc.odbc.OdbcDef;

import models.PostavkeModels;
import models.SenzoriModels;
import android.R.bool;
import android.util.Log;



public class AndroidClientControllers {

	/**
	 * modeli 
	 */

	private static SenzoriModels senzori = new SenzoriModels();
	private static PostavkeModels postavke = new PostavkeModels();

	public synchronized PostavkeModels getPostavke() {
		return postavke;
	}

	public static synchronized void setPostavke(PostavkeModels postavke) {
		AndroidClientControllers.postavke = postavke;
	}

	public SenzoriModels getSenzori() {
		return senzori;
	}

	public static void setSenzori(SenzoriModels senzori) {
		AndroidClientControllers.senzori = senzori;
	}

	/**
	 * {@code} Glavna metoda za slanje UDP datagrama
	 * @throws IOException
	 */
	@SuppressWarnings("deprecation")
	public void spajanje(boolean stanje) throws IOException{
		
		if (stanje == false) {
			InetAddress localAddress = InetAddress.getLocalHost(); // trazi lokalnu IP adresu 
			String delimiter = "."; // delimiter na 3*4 IP 
			String[] primljeno = String.valueOf(localAddress).split(delimiter); // Zapisuje "trojke" IP adrese 
			for (int i = 0; i < 244; i++) { // "vrti" zadnju trojku IP adrese, salje poziv i trazi odziv 
				InetAddress provjeraAdress = InetAddress.getByName(primljeno[0]
						+ "." + primljeno[1] + "." + primljeno[2] + "." + i);
				String sendString = new String("poziv");
				byte[] sendBuf = new byte[256];
				sendBuf = sendString.getBytes();
				DatagramSocket socket = new DatagramSocket(50001);

				//Kreira datagram paket za slanje podataka
				DatagramPacket packet = new DatagramPacket(sendBuf,
						sendBuf.length, provjeraAdress, 50001);

				Log.d("UDP", "C: Sending: '" + new String(sendBuf) + "'");

				//slanje podataka
				socket.send(packet);

				Log.d("UDP", "C: Sent.");
				Log.d("UDP", "C: Done.");

			}
			Thread odziv = new Thread(new Runnable() {

				public void run() {
					while(postavke.isSpajanje() == false){
						
						//Kreira datagram paket za prihvat podataka
			            String rcvString = "";
			            
						byte[] rcvBuf = new byte[256]; //prihvacanje podataka
						try {
							DatagramSocket socket1 = new DatagramSocket(50002);

							DatagramPacket rcvPacket = 	new DatagramPacket(rcvBuf, rcvBuf.length);

							socket1.receive(rcvPacket);

							rcvString = new String( rcvPacket.getData(),	rcvPacket.getOffset(),
									rcvPacket.getLength() );
							
							
							
							socket1.close();
							postavke.setPotvrda(rcvString);

						} catch (SocketException e) {
							// TODO Auto-generated catch block
							System.out.println("tu puknem 1");
						} catch (IOException e) {
							// TODO Auto-generated catch block
							System.out.println("tu puknem 2");
						}
						
						//System.out.println("vrijednost senzora: " + senzori.getPotvrda());

						if (rcvString.equals("odziv")) {
							
							postavke.setSpajanje(true);
							
							
							try {
								Thread.sleep(500);
							} catch (InterruptedException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							try {
								slanje();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}	

					}


				}
			}); odziv.start();
			
		}
		
	}

	public void slanje() throws IOException{
		// TODO Auto-generated method stub

		spajanje(true);
		
		try {
			// Postavka IP adrese servera
			InetAddress serverAddr = InetAddress.getByName(postavke.getServer());

			Log.d("UDP", "C: Connecting...");

			/* Kreiranje stringa za slanje */

			String paket = senzori.getPomak_x()+";"+senzori.getPomak_y()+";1"+";" + senzori.getlClick() + ";" + senzori.getrClick();
			String sendString = new String(paket);
			
			senzori.setlClick(0);
			senzori.setrClick(0);

			/*Kreiranje varijable za velicinu i slanje paketa*/
			byte[] sendBuf = new byte[256]; 
			sendBuf = sendString.getBytes();

			/*Server se nalazi po imenu racunala
			InetAddress address = InetAddress.getByName();
			senzori.setIp(address.getHostAddress());
			System.out.println("ovo saljem: " + sendString);*/
			
			//Novi UDP socket koji trazi otvoreni port servera na mrezi
			DatagramSocket socket = new DatagramSocket(50001); 
			
			//Kreira datagram paket za slanje podataka
			DatagramPacket packet =	new DatagramPacket(sendBuf, sendBuf.length,
					serverAddr, 50001);

			Log.d("UDP", "C: Sending: '" + new String(sendBuf) + "'");
			
			//slanje podataka
			socket.send( packet );
			
			Log.d("UDP", "C: Sent.");
			Log.d("UDP", "C: Done.");
		} catch (Exception e) {
			Log.e("UDP", "C: Error", e);
		}
	}





	/**
	 * {@code} Glavna metoda za primanje UDP datagrama - ne koristi se
	 * @throws IOException
	 */
	
	public void primanje() throws IOException{

		while(true){
			
			//Kreira datagram paket za prihvat podataka
            String rcvString = "";
            
			postavke.setPotvrda(rcvString);
			if(postavke.getPotvrda().equals("")== true){
				postavke.setPotvrda("0");
			}

			byte[] rcvBuf = new byte[256]; //prihvacanje podataka
			try {
				DatagramSocket socket1 = new DatagramSocket(50002);

				DatagramPacket rcvPacket = 	new DatagramPacket(rcvBuf, rcvBuf.length);

				socket1.receive(rcvPacket);

				rcvString = new String( rcvPacket.getData(),	rcvPacket.getOffset(),
						rcvPacket.getLength() );
				
				socket1.close();
				

			} catch (SocketException e) {
				// TODO Auto-generated catch block
				System.out.println("tu puknem 1");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("tu puknem 2");
			}
			

			if (rcvString.equals("1")) {
				postavke.setPotvrda("1");
				
				
				
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}	

		}

	}

}
