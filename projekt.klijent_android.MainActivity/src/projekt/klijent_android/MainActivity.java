package projekt.klijent_android;

import java.io.IOException;

import models.PostavkeModels;
import models.SenzoriModels;
import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import controllers.AndroidClientControllers;

public class MainActivity extends Activity implements SensorEventListener {

	/*
	 * Definirane varijable prikaza 
	 */
	AndroidClientControllers kontroler = new AndroidClientControllers();
	SenzoriModels senzori = kontroler.getSenzori();
	PostavkeModels postavke = kontroler.getPostavke();

	SensorManager sensorManager ;

	TextView x;
	TextView y;
	TextView z;


	EditText iP1;
	EditText iP2;
	EditText iP3;
	EditText iP4;

	Button potvrda;
	Button kalibracija;
	Button izlaz;
	Button lijevi;
	Button desni;

	boolean stisnut = false ;

	/**
	 * <{@code}>Glavna metoda
	 */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

		setContentView(R.layout.activity_main);

		try {
			kontroler.spajanje(false);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}



		/*
		 * Povezivanje varijabli prikaza sa XML-om
		 

		x=(TextView) findViewById(R.id.akceleracija_x);
		y=(TextView) findViewById(R.id.akceleracija_y);
		z=(TextView) findViewById(R.id.akceleracija_z);
		 
        x.setText("0");
        y.setText("0");
        z.setText("0");

        azim=(TextView) findViewById(R.id.azimut);
        x1=(TextView) findViewById(R.id.orjentacija_x);
        y1=(TextView) findViewById(R.id.orjentacija_y);
		 

		iP1=(EditText) findViewById(R.id.editText1);
		iP2=(EditText) findViewById(R.id.editText2);
		iP3=(EditText) findViewById(R.id.editText3);
		iP4=(EditText) findViewById(R.id.editText4); 

		
        iP1.setText("000");
        iP2.setText("000");
        iP3.setText("000");
        iP4.setText("000"); 
		 */

		//potvrda=(Button) findViewById(R.id.Ip);
		kalibracija=(Button) findViewById(R.id.kalibracija);
		izlaz=(Button) findViewById(R.id.exit);
		lijevi=(Button) findViewById(R.id.left);
		desni=(Button) findViewById(R.id.right);



		/*azim.setText("Azimuth: " + senzori.getAzimuth());
        x1.setText("Polozaj X: " + senzori.getPolozaj_x());
        y1.setText("Polozaj Y: " + senzori.getPolozaj_y());*/

		/* Novi thread za slanje datagrama */
		final Thread slanje = new Thread(new Runnable() {
			public void run() {
				while(true){
					try {
						if(postavke.isSpajanje() == true)
						kontroler.slanje();

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			}
		});slanje.start();

		/* Listener na kontroli kalibracije*/
		kalibracija.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				senzori.setKorekcija_x(0);
				senzori.setKorekcija_y(0);
				senzori.setKorekcija_z(0);

				senzori.setKorekcija_x(Math.round(Float.valueOf(String.valueOf(x.getText()))));
				senzori.setKorekcija_y(Math.round(Float.valueOf(String.valueOf(y.getText()))));
				senzori.setKorekcija_z(Math.round(Float.valueOf(String.valueOf(z.getText()))));
			}
		}); 

		/* Listener na kontroli izlaza iz aplikacije*/
		izlaz.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				System.exit(0);
			}
		});

		/* Listener na kontroli lijevog mouse klika*/
		lijevi.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				senzori.setlClick(1);
			}
		});

		/* Listener na kontroli desnog mouse klika*/
		desni.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				senzori.setrClick(1);
			}
		});

		/* Listener na kontroli potvrde IP adrese servera*/
		potvrda.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub

				//Spaja IP od 4 EditText boxa
				String iP = String.valueOf(iP1.getText())+"." + String.valueOf(iP2.getText())
						+ "." + String.valueOf(iP3.getText()) + "." + String.valueOf(iP4.getText());

				//Postavlja IP u varijablu server preko metode set
				postavke.setServer(iP);
			}
		});

		Thread senTresnje = new Thread(new Runnable() {
   		public void run() {
   			if (postavke.getPotvrda() == "1"){
   				// stvara instancu vibratora
   				Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
   			 
   			// Vibrira pola sekunde
   			v.vibrate(500);

   			}

   		}
   	});senTresnje.start();
   	
	}

	/**
	 * <{@code}>Metoda kreiranja postavki - ne koristi se 
	 */
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}


	/**
	 * <{@code}> Metode prihvata senzora - auto generirane
	 */

	//Pokretanje listenera senzora 
	protected void onResume() {
		super.onResume();

		sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL );
		// sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION), SensorManager.SENSOR_DELAY_GAME);
	}

	//Zaustavljanje listenera senzora 
	protected void onStop() {
		super.onStop();
		sensorManager.unregisterListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER));
		// sensorManager.unregisterListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION));
	}

	//Promjena stanja senzora
	public void onSensorChanged(final SensorEvent event) {
		synchronized (this) {
			/*Switch - case petlja odabira senzora*/
			switch (event.sensor.getType()){
			
			case Sensor.TYPE_ACCELEROMETER:

				/* Postavljanje dobivenih vrijednosti senzora i korekcija kalibracijom*/
				if (event.values[0]< 0) senzori.setPomak_x(Float.toString(Math.round(event.values[0]) - senzori.getKorekcija_x()));
				else senzori.setPomak_x(Float.toString(Math.round(event.values[0])  + senzori.getKorekcija_x()));

				if (event.values[1]< 0)senzori.setPomak_y(Float.toString(Math.round(event.values[1])  + senzori.getKorekcija_y()));
				else senzori.setPomak_y(Float.toString(Math.round(event.values[1])  - senzori.getKorekcija_y()));

				if (event.values[2]< 0)senzori.setPomak_z(Float.toString(Math.round(event.values[2])  + senzori.getKorekcija_z()));
				else senzori.setPomak_z(Float.toString(Math.round(event.values[2])  - senzori.getKorekcija_z()));
				
				/*Postavljanje senzora u model set metodom*/
				x.setText( String.valueOf(Math.round(Float.valueOf(senzori.getPomak_x()))));
				y.setText(String.valueOf(Math.round(Float.valueOf(senzori.getPomak_y()))));
				z.setText( String.valueOf(Math.round(Float.valueOf(senzori.getPomak_z()))));

				break;

				/*     case Sensor.TYPE_ORIENTATION:
		        	 	senzori.setAzimuth(Float.toString(event.values[0]));
		                senzori.setPolozaj_x(Float.toString(event.values[1]));
		                senzori.setPolozaj_y(Float.toString(event.values[2]));

		                azim.setText("Azimuth: " + senzori.getAzimuth());
		    	        x1.setText("Polozaj X: " + senzori.getPolozaj_x());
		    	        y1.setText("Polozaj Y: " + senzori.getPolozaj_y());

		        break; */

			}
		}

	}
	
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub

	}

}
